function cp=cp_SO2(T)
%this functions returns the cp of SO2 in J/kmol*K 

A = 3.3375E+04;
B = 2.5864E+04;
C = 9.3280E+02;
D = 1.0880E+04;
E = 4.2370E+02;

cp= A + B.*(C./T./sinh(C./T)).^2 + D.*(E./T./cosh(E./T)).^2;