%Diagrammare il cp della SO2 al variare della temperatura tra 0 e 1000�C

% il cp della SO2 alla tamperatura T � calcolato nella funzione cp_SO2(T)

T=273:10:1273; %inizializzo il vettore delle tempertaure (K)
for i=1:length(T)
    cp(i)=cp_SO2(T(i)); % assegno all'elemento i-esimo del vettore cp il cp calcolato alla temperatura in posizione i-esima di T
end

T=T-273; %sottraggo al vettore delle T 273K per convertire da K a �C

plot(T,cp) %diagrammo cp vs T