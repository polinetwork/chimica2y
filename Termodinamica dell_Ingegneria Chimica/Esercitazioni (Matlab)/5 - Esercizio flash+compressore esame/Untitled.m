clear all
clc
format compact

R=8.314;

T=343;
F=100;  %mol/h
za=0.3;
zb=0.7;
xa=0.1;
xb=0.9;

Xo=[10 0.5 0.5 50 50];
var=fsolve('sistema',Xo);
P=var(1)*10^5     %Pa
ya=var(2)
yb=var(3)
L=var(4)
V=var(5)


T2=580;
P2=10e5;

%componente B
om=0.5336;
cp=165.347;
Tc=513; Pc=81e5;

%vdw
[Z,A,B,S,k]=EoS(T2,P2,Tc,Pc,om,1);
STrk=0;
[hrRT,srR]=fr(Z(3),A,B,STrk,1);
hr2=hrRT*R*T2;

[Z,A,B,S,k]=EoS(T,P,Tc,Pc,om,1);
STrk=0;
[hrRT,srR]=fr(Z(3),A,B,STrk,1);
hr=hrRT*R*T;

w=cp*(T2-T)+hr2-hr;
disp('VdW');
W=w*V/3600


%RKS
[Z,A,B,S,k]=EoS(T2,P2,Tc,Pc,om,3);
STrk=S*sqrt(T2/(Tc*k));
[hrRT,srR]=fr(Z(3),A,B,STrk,3);
hr2=hrRT*R*T2;

[Z,A,B,S,k]=EoS(T,P,Tc,Pc,om,3);
STrk=S*sqrt(T/(Tc*k));
[hrRT,srR]=fr(Z(3),A,B,STrk,3);
hr=hrRT*R*T;

w=cp*(T2-T)+hr2-hr;
disp('RKS');
W=w*V/3600












