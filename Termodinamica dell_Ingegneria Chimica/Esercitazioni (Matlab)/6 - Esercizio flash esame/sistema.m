function f=sistema(X)

global T P F z Af w Po HO2 HN2 Tc Pc om tipo phio1 phio2

x=[X(1) X(2) X(3) 1-X(1)-X(2)-X(3)];
y=[X(4) X(5) X(6) 1-X(4)-X(5)-X(6)];
V=X(7);
L=F+Af-V;

gamma1=exp(0.5*x(2)^2);
gamma2=exp(0.5*x(1)^2);

[Z,A,B,Ap,Bp,E]=EoS_mix(T,P,y,Tc,Pc,om,tipo);
phi1=phi_mix(Z(3),A,B,Ap(1),Bp(1),tipo);
phi2=phi_mix(Z(3),A,B,Ap(2),Bp(2),tipo);

f(1)=P*y(3)-HO2*x(3);
f(2)=P*y(4)-HN2*x(4);
f(3)=P*y(1)*phi1-Po(1)*phio1*gamma1*x(1);
f(4)=P*y(2)*phi2-Po(2)*phio2*gamma2*x(2);
f(5)=F*z(1)-V*y(1)-L*x(1);
f(6)=F*z(2)-V*y(2)-L*x(2);
f(7)=Af*w(3)-V*y(3)-L*x(3);