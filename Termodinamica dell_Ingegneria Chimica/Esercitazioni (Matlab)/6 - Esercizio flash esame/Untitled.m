clear al
clc
format short g

global T P F z Af w Po HO2 HN2 Tc Pc om tipo phio1 phio2
tipo=2; %EoS RK
R=8.314;
T1=298;
T=323;
P=1.01325e5;

F=100;
z=[0.3 0.7 0 0];
Af=200;
w=[0 0 0.21 0.79];

%Antoine THF H2O
PA=[4.12118 5.20389];
PB=[1202.942 1733.926];
PC=[-46.818 -39.485];
Po=10.^(PA-PB./(T+PC))*1.01325e5;
HO2=(4.259e4)*1.01325e5;
HN2=(9.077e4)*1.01325e5;

%THF H2O O2 N2
Tc=[540 647 32.97 126.21];
Pc=[51.09 218.11 12.76 33.46]*1.01325e5;
om=0;

[Z,A,B,S,k]=EoS(T,Po(1),Tc(1),Pc(1),om,tipo);
phio1=phi_puri(Z(3),A,B,tipo);
[Z,A,B,S,k]=EoS(T,Po(2),Tc(2),Pc(2),om,tipo);
phio2=phi_puri(Z(3),A,B,tipo);

Xo=[0.2 0.75 1e-4 0.25 0.25 0.25 100];
[X,f]=fsolve('sistema',Xo);
x=[X(1) X(2) X(3) 1-X(1)-X(2)-X(3)]
y=[X(4) X(5) X(6) 1-X(4)-X(5)-X(6)]
V=X(7)
L=F+Af-V

[Z,A,B,Ap,Bp,E]=EoS_mix(T,P,y,Tc,Pc,om,tipo);
phi1=phi_mix(Z(3),A,B,Ap(1),Bp(1),tipo);
phi2=phi_mix(Z(3),A,B,Ap(2),Bp(2),tipo);
gamma1=exp(0.5*x(2)^2);
gamma2=exp(0.5*x(1)^2);

%cp H2O O2 N2
cpa=[7.701 6.713 7.44]*4.186;
cpb=[4.595e-4 -0.878e-2 -0.324e-2]*4.186;
cpc=[2.521 4.17 6.4]*10^-6*4.186;
cpTHF=18.44*4.186;

IA=quad(@(T) w(3)*(cpa(2)+cpb(2)*T+cpc(2)*T.^2)+w(4)*(cpa(3)+cpb(3)*T+cpc(3)*T.^2), T,T1);
IF=quad(@(T) z(1)*cpTHF+z(2)*(cpa(1)+cpb(1)*T+cpc(1)*T.^2), T,T1);

[Z,A,B,Ap,Bp,E]=EoS_mix(T,P,y,Tc,Pc,om,tipo);
[hrRT,srR]=fr(Z(3),A,B,E,tipo);
hrV=hrRT*R*T;

[Z,A,B,Ap,Bp,E]=EoS_mix(T,P,x,Tc,Pc,om,tipo);
[hrRT,srR]=fr(Z(1),A,B,E,tipo);
hrL=hrRT*R*T;

hrA=0; %N2 O2 gas perfetti-->miscela gas perfetti

[Z,A,B,Ap,Bp,E]=EoS_mix(T1,P,z,Tc,Pc,om,tipo);
[hrRT,srR]=fr(Z(1),A,B,E,tipo);
hrF=hrRT*R*T1;

Q=(V*hrV+L*hrL-A*(IA+hrA)-F*(IF+hrF))/3600