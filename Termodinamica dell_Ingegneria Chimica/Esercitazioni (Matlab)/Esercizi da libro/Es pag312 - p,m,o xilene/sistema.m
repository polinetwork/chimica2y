function f=sistema(X)

global K
lambda(1)=X(1);
lambda(2)=X(2);

x=[lambda(1) 1-lambda(1)-lambda(2) lambda(2)];

f(1)=K(1)-x(1)/x(2);
f(2)=K(2)-x(3)/x(2);