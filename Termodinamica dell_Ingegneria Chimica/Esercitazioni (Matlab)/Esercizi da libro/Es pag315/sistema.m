function f=sistema(X)

global P no v K

lambda(1)=X(1);
lambda(2)=X(2);
n=no+lambda*v;
x=n/sum(n);
Pi=P*x;

f(1)=K(1)-prod(Pi.^(v(1,:)));
f(2)=K(2)-prod(Pi.^(v(2,:)));