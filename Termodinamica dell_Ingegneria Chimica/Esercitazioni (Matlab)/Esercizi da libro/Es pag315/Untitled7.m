clear all
clc
format short g

global P no v K

T=1000;
P=1; %bar
R=8.314;
%CH4 H2O CO CO2 H2
no=[2 3 0 0 0];
v=[0 -1 -1 1 1; -1 -2 0 1 4];

cpa=[17.449 28.850 28.160 45.369 27.012];
cpb=[60.449 12.055 1.675 8.688 3.509]*10^-3;
cpc=[1.117 0 5.372 0 0]*10^-6;
cpd=[-7.204 0 -2.222 0 0]*10^-9;
cpe=[0 1.006 0 -9.619 0.690]*10^5;
a=cpa*v';
b=cpb*v';
c=cpc*v';
d=cpd*v';
e=cpe*v';

Dhf=[-74520 -241818 -110525 -393509 0];
Dgf=[-50460 -228572 -137169 -394359 0];

Dho=Dhf*v';
Dgo=Dgf*v';
Ko=exp(-Dgo/(R*298))

syms t1 t2 Ks1 Ks2
Ks1=Ko(1)*exp(int( (Dho(1)+int(a(1)+b(1)*t2+c(1)*t2^2+d(1)*t2^3+e(1)*t2^-2, t2,298,t1))/(R*t1^2), t1,298,T));
Ks2=Ko(2)*exp(int( (Dho(2)+int(a(2)+b(2)*t2+c(2)*t2^2+d(2)*t2^3+e(2)*t2^-2, t2,298,t1))/(R*t1^2), t1,298,T));
K(1)=eval(Ks1);
K(2)=eval(Ks2)

Xo=[-1 1];
lambda=fsolve('sistema',Xo)
n=no+lambda*v;
x=n/sum(n)