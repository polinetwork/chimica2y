function f=equazione(lambda)

global T P no v K Tc Pc om

n=no+lambda*v;
x=n/sum(n);
tipo=4;

[Z,A,B,Ap,Bp,E]=EoS_mix(T,P,x,Tc,Pc,om,tipo);
phi(1)=phi_mix(Z(3),A,B,Ap(1),Bp(1),tipo);
phi(2)=phi_mix(Z(3),A,B,Ap(2),Bp(2),tipo);
phi(3)=phi_mix(Z(3),A,B,Ap(3),Bp(3),tipo);

a=P*phi.*x;

f=K-prod(a.^v);