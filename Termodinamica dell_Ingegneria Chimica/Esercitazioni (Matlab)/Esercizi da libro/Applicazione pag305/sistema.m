function f=sistema(X)

global P v no Ks Dhs hints

T=X(1);
lambda=X(2);

n=no+lambda*v;
x=n/sum(n);
Pi=P*x;

K=eval(Ks);
hint=eval(hints);
Dh=eval(Dhs);

f(1)=K-prod(Pi.^v);
f(2)=hint+lambda*Dh;