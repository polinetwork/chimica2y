function F=vanlaar(AB)

global antA antB antC

A=AB(1);
B=AB(2);

Taz=51.2;  %�C
Paz=400;   %torr
x1az=0.399;
x2az=0.601;

Po1=10^(antA(1)+antB(1)/(antC(1)+Taz));
Po2=10^(antA(2)+antB(2)/(antC(2)+Taz));

gamma1=exp(A*x2az^2/(x2az+A*x1az/B)^2);
gamma2=exp(B*x1az^2/(x1az+B*x2az/A)^2);

F(1)=Po1*gamma1/Paz-1;
F(2)=Po2*gamma2/Paz-1;


