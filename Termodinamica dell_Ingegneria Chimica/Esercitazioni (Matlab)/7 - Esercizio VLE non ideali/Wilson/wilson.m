function F=wilson(AB)

global antA antB antC

A=AB(1);
B=AB(2);

Taz=51.2;  %�C
Paz=400;   %torr
x1az=0.399;
x2az=0.601;

Po1=10^(antA(1)+antB(1)/(antC(1)+Taz)); %torr,�C
Po2=10^(antA(2)+antB(2)/(antC(2)+Taz));

gamma1=exp(-log(x1az+A*x2az)+x2az*(A/(x1az+A*x2az)-B/(B*x1az+x2az)));
gamma2=exp(-log(x2az+B*x1az)+x1az*(B/(B*x1az+x2az)-A/(x1az+A*x2az)));

F(1)=Po1*gamma1/Paz-1;
F(2)=Po2*gamma2/Paz-1;