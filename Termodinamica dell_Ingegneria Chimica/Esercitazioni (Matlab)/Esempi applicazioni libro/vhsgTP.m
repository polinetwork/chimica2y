% calcolo volume molare e funzioni residue a T,P con diverse EoS
clc
clear all
format long
format compact

%dati per la sostanza
Tc=126.2; %K
Pc=33.94e5; %Pa
om=0.039; %fattore acentrico di Pitzer

%gas (fase=1), liquido (fase=2)
fase=1;

%assegno condizioni di T,P
T=170;
P=100*1.01325e5;
R=8.314;
RT=R*T;
RTc=R*Tc;
TR=T/Tc;
PR=P/Pc;

%1 gas perfetto
disp('gas perfetto ');
v=R*T/P
hr=0
sr=0
gr=0


%2 vdW, RK, RKS, PR
eq=['vdW';'RK ';'RKS';'PR '];     %nomi EoS
for tipo=1:4                    %ciclo sulle diverse EoS
    [Z,A,B,S,k]=EoS(T,P,Tc,Pc,om,tipo);
    
    STrk=S*sqrt(TR/k);              %calcolo Z(P,T) 
    disp(eq(tipo, : ))              %visualizzo nome EoS    
    if fase==1                  
        v=Z(3)*RT/P        %calcolo v gas
        [hrRT,srR]=fr(Z(3),A,B,STrk,tipo);      %calcolo hr/RT e sr/R
        hr=hrRT*RT
        sr=srR*R
        gr=hr-T*sr
    else
        v=Z(1)*RT/P            %calcolo v liquido
        [hrRT,srR]=fr(Z(1),A,B,STrk,tipo);      %calcolo hr/RT e sr/R
        hr=hrRT*RT
        sr=srR*R
        gr=hr-T*sr
    end
    
end
