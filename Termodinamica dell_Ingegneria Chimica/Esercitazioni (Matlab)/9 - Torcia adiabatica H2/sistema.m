function f=sistema(X)

global P v no Ks a b c ao bo co Dho To

T=X(1);
lambda=X(2);

n=no+lambda*v;
x=n/sum(n);
Pi=P*x;

K=eval(Ks);
Dh=Dho+quad(@(t1) a+b*t1+c*t1.^-2, 298,T);
hint=quad(@(t1) ao+bo*t1+co*t1.^-2, To,T);

f(1)=K-prod(Pi.^v);
f(2)=hint+lambda*Dh;