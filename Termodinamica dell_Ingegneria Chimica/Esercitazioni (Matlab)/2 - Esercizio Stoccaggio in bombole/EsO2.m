clc
clear all

V=0.05;
P=1e7;
T=298;
R=8.314;

om=0.022;
Tc=154.6;
Pc=50.43e5;

TR=T/Tc;
PR=P/Pc;

% gas perfetto
n=P*V/(R*T)*0.032;
disp(['Gas perfetto: n=', num2str(n)]);

% viriale
B0=0.083-0.422/TR^1.6;      %calcolo B0
B1=0.139-0.172/TR^4.2;      %calcolo B1
BPcRTc=B0+om*B1;            %calcolo BPc/RTc
Z=1+BPcRTc*PR/TR;           %calcolo Z
v=Z*R*T/P;
n=V/v*0.032;
disp(['Viriale: n=', num2str(n)]);

%RK
TR=T/Tc;
    a=0.42748*(R*Tc)^2/(Pc*sqrt(TR));
    b=0.08664*R*Tc/Pc;
    A=a*P/(R*T)^2;
    B=b*P/(R*T);
    alfa=-1;
    beta=A-B-B^2;
    gamma=-A*B;
    coeff=[1 alfa beta gamma];
    Z=roots(coeff);
    v=Z(1)*R*T/P;
    n=V/v*0.032;
    disp(['RK: n=', num2str(n)]);
    
 %RKS
 TR=T/Tc;
    S=0.48+1.574*om-0.176*om^2;
    k=(1+S*(1-sqrt(TR)))^2;
    a=0.42748*k*(R*Tc)^2/Pc;
    b=0.08664*R*Tc/Pc;
    A=a*P/(R*T)^2;
    B=b*P/(R*T);
    alfa=-1;
    beta=A-B-B^2;
    gamma=-A*B;
    coeff=[1 alfa beta gamma];
    Z=roots(coeff);
    v=Z(1)*R*T/P;
    n=V/v*0.032;
    disp(['RKS: n=', num2str(n)]);
    
    
    
    
    
    
    
    
    
    