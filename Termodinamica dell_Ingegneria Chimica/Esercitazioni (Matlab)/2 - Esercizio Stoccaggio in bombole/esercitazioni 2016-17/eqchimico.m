function f=eqchimico(l) 
global n0 nu Keq_VH p p0 H
lambda=l(1);
l1=l(1);
l2=l(2);
l3=l(3);
l4=l(4);%cOnsidero eq fisico come chimico

%n=n0+lambda.*nu; NOOOOO devo separare le due fasi!!!!!!!!!!
nv(1)=n0(1)+(nu(1)*lambda)+l1;  %H2L-->H2V l1
nv(2)=n0(2)+(nu(2)*lambda)+l2;
nv(3)=n0(1)+(nu(3)*lambda)+l3;
nv(4)=n0(1)+(nu(4)*lambda)+l4;
ntotv=sum(nv);
y=nv/ntotv;

nl(1)=-l1;
nl(2)=-l2;
nl(3)=-l3;
nl(4)=-l4;
nltot=sum(nl);
x=nl/nltot;%vettore

Keq_att=prod(y.^nu);
%definisco incognite
f(1)=Keq_VH-Keq_att;
f(2)=H(1)*x(1)-p*y(1); %eq fisici
f(3)=H(2)*x(2)-p*y(2);
f(4)=H(3)*x(3)-p*y(3);
f(5)=p0(4)*x(4)-p*y(4);
