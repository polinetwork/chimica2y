clc
clear all
global Tr Tsist pr psist R V

%dati:
psit=4; %atm
Tsist=450; %K
pr=1; %atm
Tr=298; %K
R=8.314;
RT=R*Tsist;
V=[-3 -1 2];

%calori specifici parametri
cpA=[6.24 6.529 6.5846]*4.186;
cpB=[0.1039 0.1488 0.61251]*10^-2*4.186;
cpC=[-0.007804 -0.02271 0.23663]*10^-5*4.186;
cpD=[0 0 -1.5981]*10^-9*4.186;
K=(((2l)^2)/(1-l)*((1-3l)^3)*(3-2l)^2)*(1/(psist/pr)^2);
ln(K)=(-1/RT)*(DG0R);

DG0R=sum(V.*(Dg0F));

Dg0F=Dg0rif-q1/R;
Dg0rif=[0 0 -16.34];
q1=quadl('int1',Trif,Tsist);

Dh0F=Dh0rif+q2;
q2=quadl('int2',Trif,T);
Dh0rif=[0 0 -45.88];
DH0R=sum(V.*(Dh0F));