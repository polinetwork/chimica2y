f=eqchimico(x) %sistema

n=n0+lambda.*nu;
ntot=sum(n);
y=n/ntot;
Keq_att=prod(y.^nu);

%definisco incognite
x1=x(1); %composiz fase l
x2=x(2);
x3=x(3);
x4=x(4);
lambda=x(5);
y1=x(6); %composiz fase vapore
y2=x(7);
y3=x(8);
y4=x(9);

f(1)=Keq_VH-Keq_att;
f(2)=H(1)*x(1)-p*y(1); %eq fisici
f(3)=H(2)*x(2)-p*y(2);
f(4)=H(3)*x(3)-p*y(3);
f(5)=p0(4)*x(4)-p*y(4);
f(6)=n(1)-n0(1)-lambda*nu(1);%bilanci singoli
f(7)=n(2)-n0(2)-lambda*nu(2);
f(8)=n(3)-n0(3)-lambda*nu(3);
f(9)=n(4)-n0(4)-lambda*nu(4);
