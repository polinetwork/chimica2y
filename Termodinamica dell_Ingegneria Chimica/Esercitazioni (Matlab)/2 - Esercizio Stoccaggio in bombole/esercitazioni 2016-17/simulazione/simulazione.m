clc
clear all

global p T R RT Tc Pc RTc P0 H phip a b A B alfa beta gamma phim 

%THF=1; H20=2; 02=3; N2=4;
%cerco x=[x1 x2 x3 x4] con %l e y=[y1 y2 y3 y4] con %v -->xv=y e xl=x;
%parametri del sistema:
T=273+40+2*2; %K
p=1;%atm
R=0.082; %atm*l/mol*K
RT=R*T;
%parametri critici PURI:
Tc=[540 647 32.97 126.21];%K   NB vedo che T>Tc quindi sono supercritici
Pc=[51.09 218.11 12.76 33.46];%atm
RTc=R.*Tc;
%variabili ridotte:
Tr=T./Tc;
Pr=p./Pc;

%pressione di vapore SOLO PER 1 E 2 SUBCRITICI:
antA=[4.12118 5.20389 NaN NaN];
antB=[1202.942 17733.926 NaN NaN];
antC=[-46.818 -39.485 NaN NaN];
P0=10.^(antA-(antB./(T+antC)));
%per i SUPERCRITICI 3 e 4 uso Henry:
H=[NaN NaN 4.259e4 9.077e4];

%parametri della RK per 1 e 2 PURI (IN FORMA VETTORIALE):
a=(0.42748.*(RTc).^2)./(Pc.*(Tr).^1/2);
b=(0.08664.*RTc)./Pc;
A=a.*P0/(RT).^2; %NB devo calcolarli a p=P0
B=b.*P0/(RT);
alfa=-1;
beta=A-B-B.^2;
gamma=-A.*B; %SONO VETTORI

%calcolo di Zv per 1 e 2 PURI: 
%NB per i puri sono diversi perch� hanno diverse
%Tc e Pc 
coeff1=[1 alfa beta(1) gamma(1)];  %NB NON PUOI FARE TUTTO IN UN UNICO VETTORE!
Z1=roots(coeff1);
Zv1=max(Z1);

coeff2=[1 alfa beta(2) gamma(2)];
Z2=roots(coeff2);
Zv2=max(Z2);

Zv=[Zv1 Zv2 1 1];%3 e 4 sono gas perfetti

%calcolo di phip per 1 e 2 PURI:
phip = exp(Zv-1-(A./B).*log((Zv+B)./Zv)-log(Zv-B)); %ottengo quattro risultati ma 3 e 4 si discostano pochissimo da 1

%calcolo di phim per 1 e 2 in MISCELA-->nella function perch� dipendono da
%y, incognita della function.

%risolvo il sistema:
options=optimset('display','iter','tolfun',1e-10,'maxiter',3e+3,'maxfunevals',10000);
[d,fval,exitflag]=fsolve('simul',[150; 150; 0.499; 0.499; 0.001; 0.001; 0.001; 0.001; 0.499; 0.499;],options); %cos� risolve tenendo conto delle opzioni che ho definito prima

disp(['La portata di liquido �',num2str(d(1)),'mol/h']);%2str1 riporta il risultato numerico che ottiene facnedo girare il sistema poi ci aggiunge l'etichetta
disp(['La portata di vapore �',num2str(d(2)),'mol/h']);
disp(['La frazione molare x1 di liquido �',num2str(d(3))]);
disp(['La frazione molare x2 di liquido �',num2str(d(4))]);
disp(['La frazione molare x3 di liquido �',num2str(d(5))]);
disp(['La frazione molare x4 di liquido �',num2str(d(6))]);
disp(['La frazione molare y1 di vapore �',num2str(d(7))]);
disp(['La frazione molare y2 di vapore �',num2str(d(8))]);
disp(['La frazione molare y3 di vapore �',num2str(d(9))]);
disp(['La frazione molare y4 di vapore �',num2str(d(10))]);
phim 
