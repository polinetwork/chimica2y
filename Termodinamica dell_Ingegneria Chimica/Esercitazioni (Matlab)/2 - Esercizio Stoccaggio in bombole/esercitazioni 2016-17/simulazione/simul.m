function f=simul(x)
global  p T R RT Tc Pc RTc P0 H phip a b A B alfa phim 

%chiamo le incognite con x(indice): NB SCRIVO PRIMA L=X PERCHE' STO
%DICHIARANDO UNA VARIABILE CHE IL COMPUTER NON SA!
L=x(1);
V=x(2);
x1=x(3);
x2=x(4);
x3=x(5);
x4=x(6);
y1=x(7);
y2=x(8);
y3=x(9);
y4=x(10);

gamma1=exp(0.5*(x2.^2));
gamma2=exp(0.5*(x1.^2));

%calcolo phim per 1 e 2 in MISCELA: NB SONO DIVERSI PER I DUE COMPOSTI
%PERCHE' DIPENDONO ANCHE DA PC E TC
y=[y1 y2 y3 y4];
am=(sum(y.*sqrt(a)))^2; %NB QUESTO E' UNO SCALARE!!!! uso y perch� ho una miscela in fase vapore
bm=sum(y.*b); %NB dipendono anche da y, che � incognita   
Am=(am*p)/(RT)^2; %SONO SCALARI!!
Bm=(bm*p)/(RT);
betam=Am-Bm-(Bm)^2;
gammam=-Am*Bm; %SONO SCALARI, UNICI PER TUTTI I COMPONENTI DELLA MISCELA!

coeffm=[1 alfa betam gammam];  %NB NON PUOI FARE TUTTO IN UN UNICO VETTORE!
Zm=roots(coeffm);
Zvm=max(Zm);%in miscelaho solo 1 z! NON SONO VETTORI PERCHE' COSTITUITI DA SCALARI!

phim = exp((B./Bm).*(Zvm-1)-log(Zvm-Bm)+(Am./Bm).*((B./Bm)-2.*sqrt(A./Am)).*log((Zvm+Bm)./Zvm));


%scrivo le equazioni del sistema in forma matriciale:
f=[L-0.084+V; %1BMG
0.0084-V*y1-L*x1; %NC BMSi
0.0196-V*y2-L*x2;
 0.0112-V*y3-L*x3; 
0.0448-V*y4-L*x4;
 x1+x2+x3+x4-(y1+y2+y3+y4);%1 STECH
(P0(1)*phip(1)*x1*gamma1)-(p*y1*phim(1));%NC VLE
(P0(2)*phip(2)*x2*gamma2)-(p*y2*phim(2));
 (H(3)*x3)-(p*y3);
 (H(4)*x4)-(p*y4);]