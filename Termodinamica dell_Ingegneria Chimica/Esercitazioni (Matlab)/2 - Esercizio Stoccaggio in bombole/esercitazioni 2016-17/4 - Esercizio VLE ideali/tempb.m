function Tb=tempb(T)

global P A B C x



Po(1)=10^(A(1)+B(1)/(T+C(1)));       %P torr, T �C
Po(2)=10^(A(2)+B(2)/(T+C(2)));       %P torr, T �C

Tb=(Po(1)*x(1)+Po(2)*x(2))/(P*760)-1;


