function F=ris_PR2(X)

global a b R T

P=X;

%calcolo A e B 
A = (a*P)/((R*T)^2);
B = (b*P)/(R*T);

%parametri cubica
alf = -1+B;
bet = A-2*B-3*(B^2);
gam = -A*B+(B^2)+(B^3);

%Soluzione cubica

Z = roots([1 alf bet gam]);

ZR = [];
for i = 1:3
   if isreal(Z(i))
   	ZR = [ZR Z(i)];   
   end
end
sort(ZR);
Zl = min(ZR);   
Zv = max(ZR);
   

%Phi fase liquida
phi_l =exp(Zl-1-(A/(2*sqrt(2)*B))*...
    log((Zl+B*(1+sqrt(2)))/(Zl+B*(1-sqrt(2))))-log(Zl-B));
%Phi fase liquida
phi_v =exp(Zv-1-(A/(2*sqrt(2)*B))*...
    log((Zv+B.*(1+sqrt(2)))./(Zv+B.*(1-sqrt(2))))-log(Zv-B));

F = phi_l-phi_v;
