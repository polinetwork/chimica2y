clear all
clc
format short g

global Tc Pc om sr2 tipo

Tc=283.1;
Pc=51.17e5;
om=0.089;
R=8.314;

T2=298; P2=1.01325e5;

tipo=4; %PR
[Z,A,B,S,k]=EoS(T2,P2,Tc,Pc,om,tipo);
E=S*sqrt(T2/(Tc*k));
[hrRT,srR]=fr(Z(3),A,B,E,tipo);
sr2=srR*R;
hr2=hrRT*R*T2;

T3=fsolve('temp',800)
P3=200*1.01325e5;
[Z,A,B,S,k]=EoS(T3,P3,Tc,Pc,om,tipo);
E=S*sqrt(T3/(Tc*k));
[hrRT,srR]=fr(Z(3),A,B,E,tipo);
sr3=srR*R;
hr3=hrRT*R*T3;

w=quad(@(T) 4.196+154.565e-3*T-81.076e-6*T.^2+16.813e-9*T.^3 , T2,T3)+hr3-hr2

T4=313; P4=P3;
[Z,A,B,S,k]=EoS(T4,P4,Tc,Pc,om,tipo);
E=S*sqrt(T4/(Tc*k));
[hrRT,srR]=fr(Z(1),A,B,E,tipo);
hr4=hrRT*R*T4;

q=quad(@(T) 4.196+154.565e-3*T-81.076e-6*T.^2+16.813e-9*T.^3 , T4,T3)+hr3-hr4

T7=169.4; P7=P2;
[Z,A,B,S,k]=EoS(T7,P7,Tc,Pc,om,tipo);
E=S*sqrt(T7/(Tc*k));
[hrRT,srR]=fr(Z(3),A,B,E,tipo);
hr7=hrRT*R*T7-3237*4.186;

T8=T2; P8=P2;
hr8=hr2;

I48=quad(@(T) 4.196+154.565e-3*T-81.076e-6*T.^2+16.813e-9*T.^3 , T8,T4);
I78=quad(@(T) 4.196+154.565e-3*T-81.076e-6*T.^2+16.813e-9*T.^3 , T8,T7);

x=(I48+hr4-hr8)/(I78+hr7-hr8)













