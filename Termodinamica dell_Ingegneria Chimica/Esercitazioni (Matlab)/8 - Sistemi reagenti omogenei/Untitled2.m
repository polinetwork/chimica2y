clear all
clc
format short g

global KT P n0 v

P=4; %atm
T=450;

R=8.314;

%3 H2 + N2 --> 2NH3    
n0=[1 1 0]; % +1mol inerte
v=[-3 -1 2];

%calori specifici parametri
cpA=[6.24 6.529 6.5846]*4.186;
cpB=[0.1039 0.1488 0.61251]*10^-2*4.186;
cpC=[-0.007804 -0.02271 0.23663]*10^-5*4.186;
cpD=[0 0 -1.5981]*10^-9*4.186;

%h e g di formazione (298K) J/molK
Dhf=[0 0 -45.88];
Dgf=[0 0 -16.34];

Dho=Dhf*v';
Dgo=Dgf*v';
Ko=exp(-Dgo/(R*298))

am=cpA*v';
bm=cpB*v';
cm=cpC*v';
dm=cpD*v';

syms t1 t2 KTs 
KTs=Ko*exp(int( (Dho+int(am+bm*t2+cm*t2^2+dm*t2^3, t2,298,t1))/(R*t1^2), t1,298,T));
KT=eval(KTs)

lambda=fsolve('equazione',0.2)

n=n0+lambda*v;
disp('H2 N2 NH3');
x=n/(sum(n)+1)
disp('diluente inerte');
xinerte=1-sum(x)