function f=sistema(X)

global no v K

na=[X(1) X(2) X(3) 0 no(5) 0];
nb=[0 X(4) X(5) X(6) 0 no(6)];
ntota=sum(na);
ntotb=sum(nb);
xa=na/ntota;
xb=nb/ntotb;

lambda=[X(7) X(8)];
n=no+lambda*v;

G=1.5;
gammaa=[1 exp(G*xa(3)^2) exp(G*xa(2)^2) 1 1 1];    
gammab=[1 exp(G*xb(3)^2) exp(G*xb(2)^2) 1 1 1]; 

aa=gammaa.*xa;
ab=gammab.*xb;

f(1)=n(1)-na(1)-nb(1);
f(2)=n(2)-na(2)-nb(2);
f(3)=n(3)-na(3)-nb(3);
f(4)=n(4)-na(4)-nb(4);
f(5)=K(1)-prod(aa.^v(1,:));
f(6)=K(2)-prod(ab.^v(2,:));
f(7)=aa(2)-ab(2);
f(8)=aa(3)-ab(3);




