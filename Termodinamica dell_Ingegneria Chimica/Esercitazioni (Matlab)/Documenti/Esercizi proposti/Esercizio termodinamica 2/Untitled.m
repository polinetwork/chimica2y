clear all
clc
format short g

global no v K

R=8.314;
T=383;
P=5; %bar

%[ A B C P S1 S2]
no=[92.592 104.167 0 0 250 98.684];
v=[-2 1 1 0 0 0;
    0 -1 -1 1 0 0];

dg(1)=-12530+86*(T-273);
dg(2)=-15770+112*(T-273);

K=exp(-dg./(R*T))

Xo=[50 50 10 50 5 20 20 10];
X=fsolve('sistema',Xo);
lambda=[X(7) X(8)]
na=[X(1) X(2) X(3) 0 no(5) 0]
nb=[0 X(4) X(5) X(6) 0 no(6)]
ntota=sum(na);
ntotb=sum(nb);
xa=na/ntota
xb=nb/ntotb