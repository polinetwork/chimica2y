clear all
clc 
format short g

global T T1 P1 F z x Po v no K

T=343;
T1=393;
P1=1; %atm
F=100; %mol/h
z=[0.3 0.7 0];
x=[0.1 0.9 0];

PA=[7.50211 7.87863];
PB=[-1132.83 -1463.11];
PC=[228 230];
Po=10.^(PA+PB./(T-273+PC))/760;

Xo=[3 0.5 50]; 
X=fsolve('flash',Xo);
P=X(1)
y=[X(2) 1-X(2) 0]
V=X(3)
L=F-V
alfa=V/F

%A+B-->2C
v=[-1 -1 2];
no=V*y;
Dgf=[-25000 -30000 -25000]*4.186;
Dhf=[-40000 -50000 -34000]*4.186;
cpi=[5.5 7.5 9.6]*4.186;
cp=cpi*v';

R=8.314;

Dho=Dhf*v';
Dgo=Dgf*v';

Ko=exp(-Dgo/(R*298));
K=Ko*exp( quad(@(t) (Dho+cp*(t-298))./(R*t.^2),298, T1) )

lambda=fsolve('reattore', 15)
n=no+lambda*v;
w=n/sum(n)

cpo=no*cpi';
DHr=Dho+cp*(T1-298);

Q=(cpo*(T-T1)-lambda*DHr)/3600 %positivo uscente