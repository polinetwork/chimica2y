function f=flash(X)

global F z x Po 

P=X(1);
y=[X(2) 1-X(2) 0];
V=X(3);
L=F-V;

gamma1=exp(0.5*x(2)^2);
gamma2=exp(0.5*x(1)^2);

f(1)=P*y(1)-Po(1)*x(1)*gamma1;
f(2)=P*y(2)-Po(2)*x(2)*gamma2;
f(3)=F*z(1)-V*y(1)-L*x(1);