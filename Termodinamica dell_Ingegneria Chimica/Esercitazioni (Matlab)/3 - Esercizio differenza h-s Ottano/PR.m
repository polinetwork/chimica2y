function [Z,A,B,S,k]=PR(T,P,Tc,Pc,om)

R=8.314;

S=0.37464+1.54226*om-0.26992*om^2;
k=(1+S*(1-sqrt(T/Tc)))^2;
a=(0.45724*(R*Tc)^2*k)/Pc;
b=(0.07780*R*Tc)/Pc;

A=a*P/(R*T)^2;
B=b*P/(R*T);

alfa=-1+B;
beta=A-2*B-3*B^2;
gamma=-A*B+B^2+B^3;

coeff=[1 alfa beta gamma];
Z=roots(coeff);
Z=sort(Z);
