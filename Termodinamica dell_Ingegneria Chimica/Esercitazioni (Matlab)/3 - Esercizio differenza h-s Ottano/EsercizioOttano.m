clear all
clc
format short g
format compact

Tc=569.4;
Pc=24.97e5;
om=0.398;

T1=427.9;
P1=2.15e5;      %vapore
T2=427.9;
P2=5e5;         %liquido
R=8.314;

Ih=R*quad(@(T) 8.163+70.567e-3*T-22.208e-6*T.^2, T1,T2);
Is=R*quad(@(T) (8.163+70.567e-3*T-22.208e-6*T.^2)./T, T1,T2);

%stato 1 vapore
[Z,A,B,S,k]=PR(T1,P1,Tc,Pc,om);
E=S*sqrt(T1/(Tc*k));
[hrRT,srR]=frPR(Z(3),A,B,E);
hr1=hrRT*R*T1;
sr1=srR*R;

%stato 2 liquido
[Z,A,B,S,k]=PR(T2,P2,Tc,Pc,om);
E=S*sqrt(T2/(Tc*k));
[hrRT,srR]=frPR(Z(1),A,B,E);
hr2=hrRT*R*T2;
sr2=srR*R;


deltah=Ih+hr2-hr1
deltas=Is-R*log(P2/P1)+sr2-sr1







    