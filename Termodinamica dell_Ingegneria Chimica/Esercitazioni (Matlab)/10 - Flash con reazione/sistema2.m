function f=sistema2(X)

global P Po K F z v

x=[X(1) X(2) X(3) 1-X(1)-X(2)-X(3)];
y=[X(4) X(5) X(6) X(7)];
V=X(8);
L=F-V;
lambda=X(9);

f(1)=P*y(1)-Po(1)*x(1);
f(2)=P*y(2)-Po(2)*x(2);
f(3)=P*y(3)-Po(3)*x(3);
f(4)=P*y(4)-Po(4)*x(4);
f(5)=F*z(1)+lambda*v(1)-V*y(1)-L*x(1);
f(6)=F*z(2)+lambda*v(2)-V*y(2)-L*x(2);
f(7)=F*z(3)+lambda*v(3)-V*y(3)-L*x(3);
f(8)=F*z(4)+lambda*v(4)-V*y(4)-L*x(4);
f(9)=K-prod(x.^v);