function f=sistema(X)

global P Po K F z v

V=X(1);
L=F-V;
lambda=X(2);

x=(F*z+lambda*v)./(V*Po/P+L);

f(1)=sum(x)-1;
f(2)=prod(x.^v)-K;