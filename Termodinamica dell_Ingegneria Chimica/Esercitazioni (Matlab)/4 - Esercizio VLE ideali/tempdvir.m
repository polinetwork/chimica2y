function Td=tempdvir(T)

global P A B C Tc Pc om x
R=8.314;

Po(1)=10^(A(1)+B(1)/(T+C(1)));       %P torr, T �C
Po(2)=10^(A(2)+B(2)/(T+C(2)));       %P torr, T �C

%componente 1
TR1=(T+273)/Tc(1);
PR1=P/Pc(1);
PRo1=Po(1)/(Pc(1)*760);
F01=0.1445-0.33/TR1-0.1385/TR1^2-0.0121/TR1^3-0.000607/TR1^8;
F11=0.0637+0.331/TR1^2-0.423/TR1^3-0.008/TR1^8;
BPcRTc1=F01+om(1)*F11;
BPRT1=BPcRTc1*PR1/TR1;
BPRTo1=BPcRTc1*PRo1/TR1;

phio1=exp(BPRTo1);

%componente 2
TR2=(T+273)/Tc(2);
PR2=P/Pc(2);
PRo2=Po(2)/(Pc(2)*760);
F02=0.1445-0.33/TR2-0.1385/TR2^2-0.0121/TR2^3-0.000607/TR2^8;
F12=0.0637+0.331/TR2^2-0.423/TR2^3-0.008/TR2^8;
BPcRTc2=F02+om(2)*F12;
BPRT2=BPcRTc2*PR2/TR2;
BPRTo2=BPcRTc2*PRo2/TR2;

phio2=exp(BPRTo2);

B1=BPRT1*(R*T/P);
B2=BPRT2*(R*T/P);
B12=(B1+B2)/2;
Bm=B1*x(1)^2+B2*x(2)^2+2*B12*x(1)*x(2);

phi1=exp(P/(R*T)*(-Bm+2*(x(1)*B1+x(2)*B12)));
phi2=exp(P/(R*T)*(-Bm+2*(x(1)*B12+x(2)*B2)));

Td=P*760*((x(1)*phi1)/(Po(1)*phio1)+(x(2)*phi2)/(Po(2)*phio2))-1;





