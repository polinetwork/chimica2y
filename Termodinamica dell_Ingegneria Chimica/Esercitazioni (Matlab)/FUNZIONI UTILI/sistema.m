function f=sistema(X)

global z F alfa P Tc Pc om tipo

T=X(1);
y=[X(2) X(3) X(4) 1-X(2)-X(3)-X(4)];
x=[X(5) X(6) X(7) 1-X(5)-X(6)-X(7)];

[Z,A,B,Ap,Bp,a,b,am,bm,E]=EoS_mix(T,P,x,Tc,Pc,om,tipo);
phiL=(1/(Z(1)-B)).*(1+B/Z(1)).^(-A/B.*(2*sqrt(a./am)-b./bm)).*exp((Z(1)-1)*b./bm);

[Z,A,B,Ap,Bp,a,b,am,bm,E]=EoS_mix(T,P,y,Tc,Pc,om,tipo);
phiV=(1/(Z(3)-B)).*(1+B/Z(3)).^(-A/B.*(2*sqrt(a./am)-b./bm)).*exp((Z(3)-1)*b./bm);

V=alfa*F;
L=F-V;

f(1)=F*z(1)-V*y(1)-L*x(1);
f(2)=F*z(2)-V*y(2)-L*x(2);
f(3)=F*z(3)-V*y(3)-L*x(3);
f(4)=y(1)*phiV(1)-x(1)*phiL(1);
f(5)=y(1)*phiV(1)-x(1)*phiL(1);
f(6)=y(1)*phiV(1)-x(1)*phiL(1);
f(7)=y(1)*phiV(1)-x(1)*phiL(1);