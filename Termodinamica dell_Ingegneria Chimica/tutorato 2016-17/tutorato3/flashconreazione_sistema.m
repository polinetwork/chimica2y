function y=flashconreazione_sistema(x)
%NB dichiaro prima le variabili che NON CONOSCE perch� le x le ho gi�
%dichiarate nella function

global F za zb zc zw P T R P0A P0B P0C P0W DG0R 

L=x(1);
V=x(2);
lambda=x(3);
xa=x(4);
xb=x(5);
xc=x(6);
xw=x(7);
ya=x(8);
yb=x(9);
yc=x(10);
yw=x(11);

%ora scrivo altrettante equazioni(da azzerare):
y(1)=F*za-lambda-V*ya-L*xa;%BILANCI MAT SINGOLI E TOT
y(2)=F*zb-lambda-V*yb-L*xb;
y(3)=F*zc+lambda-V*yc-L*xc;
y(4)=F*zw+lambda-V*yw-L*xw;
y(5)=F-V-L;%NB solo perch� avviene senza variazione del num di moli
y(6)=P*ya-P0A*xa; %4 EQ FISICI
y(7)=P*yb-P0B*xb; 
y(8)=P*yc-P0C*xc; 
y(9)=P*yw-P0W*xw; 
y(10)=xa+xb+xc+xw-1; %1 STECHIOMETRICA
y(11)=(xc*xw/(xa*xb))-exp(-DG0R/(R*T)); %EQ CHIMICO 

%NB SOLO ORA VADO A SCRIVERE IL global INDICANDOGLI LE VARIABILI CHE HO
%GIA' DEFINITO NEL MAIN tutte quelle che compaiono nelle y e che non ho
%definito come x!

