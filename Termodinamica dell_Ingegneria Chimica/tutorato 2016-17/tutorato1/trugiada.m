function TR= trugiada(T)

global A B C p x   %per recuperare le variabili dall'altro script

PO(1)=(10^(A(1)+B(1)/(T+C(1))))/760; %p in atm; T in °C
PO(2)=(10^(A(2)+B(2)/(T+C(2))))/760;

TR= p*(1/PO(1)*x(1) +(1/PO(2))*x(2)) -1
end