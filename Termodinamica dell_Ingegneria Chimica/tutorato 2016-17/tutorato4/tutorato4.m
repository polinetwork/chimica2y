clear all
close all
clc
%%dati NC=4
P=1; %atm
Trif=298; 
R=1.987; %[cal/mol K]
n0=[1; 0.5; 0; 2] %tengo conto anche dell'inerte nel vettore di moli iniziali 
%[H2 02 H20 N2]
cs=[-1;-0.5;1;0] %coeff stechiometrici
Acp=[6.524, 6.148, 7.256, 6.83]; %[cal/mol K]
Bcp=(1e-2).*[0.125, 0.3102, 0.23, 0.09];
Ccp=(1e-5).*[0, 0, 0, 0];
Dcp=(1e-5).*[0, 0, 0, 0];
Ecp=(1e5).*[0, 0, 0, 0.12];

G0frif=[0,0,-54640,0]; %[cal/mol]
H0frif=[0,0,-57800,0];%[cal/mol]

%%SOLUZIONI : dato che ho un integrale con T variabile ricorro a variabili
%%simboliche e ciclo for 
syms lambda T %uso queste due variabili
n=n0+(lambda).*cs %vettore delle moli a equilibrio (operazione tra vettori)
N=length (n0); %mi da il numero delle specie tenendo conto dell'inerte

%ciclo: definisco prima contatore
cont=0;
for i=1:N %ripete il ciclo per tutte le specie fino a N 
cont=cont+n(i) %cont dopo l'= � il primo che ho def come 0 (prende il precedente) poi n � funzione di i che cicla da 1 a 4
end
ntot=cont %(ho definito n con il contatore, sommando tutte le specie), QUESTO E' IN FUNZ DI LAMBDA!!!

deltacpA=Acp*cs;
deltacpB=Bcp*cs;
deltacpC=Ccp*cs;
deltacpD=Dcp*cs;
deltacpE=Ecp*cs;

%VAN'T HOFF (calcolo deltaG0R e deltaH0R a Trif e Prf)
%o faccio cos�: G0Rrif=sum(G0frif*cs) oppure ciclo
contb=0;
for i=1:N
contb=contb+(cs(i)*G0frif(i));
end
G0Rrif=contb;

contc=0;
for i=1:N
contc=contc+(cs(i)*H0frif(i));
end
H0Rrif=contc;

%ora voglio in funz di T usando la funzione cp(T)=A+B*T+C*T^2+D*T^3+E/T^2
syms T1 T2 %per calcolare i due integrali
Cptot=deltacpA+deltacpB*T+deltacpC*(T^2)+deltacpD*(T^3)+deltacpE*(T^-2);%funzione integranda
deltaH0RdiT=H0Rrif+int(Cptot,T,Trif,T1);%(funz,variabile,estremi di integrazione) T1 non conosco!

%MA IL FRATTO RT^2?!!!!!
integrale=deltaH0RdiT/(R*T1^2); %funz integranda (a sua volta inetgrale)
I=int(integrale,T1,Trif,T2);

%calcolo Keqrif
Keqrif=exp(-G0Rrif/(R*Trif));

%ora mi occupo delle attivit�:
y=(1/ntot)*n; %definisco y IN FUNZIONE DI LAMBDA

contd=1; %ciclo solo fino a N-1 perch� ho l'inerte
for i=1:N-1
contd=contd*((P*y(i))^cs(i)); %col ciclo non ho problema delle operazioni vettoriali
end
produttoria=contd
Keq=produttoria


eq1=log(Keq/Keqrif)-I %VAN'T HOFF
%l'altra equazione che mi serve � bilancio en:

syms T3 %uso T3 come variabile e definisco i Cp(T3) separatamente perch� prima li avevo raggruppati per Cptot:
CpH2=Acp(1)+Bcp(1)*T3+Ccp(1)*(T3^2)+Dcp(1)*(T3^3)+Ecp(1)*(T3^-2);
CpO2=Acp(2)+Bcp(2)*T3+Ccp(2)*(T3^2)+Dcp(2)*(T3^3)+Ecp(2)*(T3^-2);
CpH2O=Acp(3)+Bcp(3)*T3+Ccp(3)*(T3^2)+Dcp(3)*(T3^3)+Ecp(3)*(T3^-2);
CpN2=Acp(4)+Bcp(4)*T3+Ccp(4)*(T3^2)+Dcp(4)*(T3^3)+Ecp(4)*(T3^-2);

%definisco anche gli integrali separatamente, poi li dovr� sommare
% NB T1 INCOGNITA e T3 variabile
intH2=int(CpH2,T3, Trif, T1);
intO2=int(CpO2,T3, Trif, T1);
intH2O=int(CpH2O,T3, Trif, T1);
intN2=int(CpN2,T3, Trif, T1);
%ora li sommo:
eq2=n0(1)*intH2+n0(2)*intO2+n0(3)*intH2O+intN2*n0(4)+lambda*deltaH0RdiT;
eq3=T1-T2; %scrivo io un'altra equazione per avere le variabili vincolate (ho definito variabili simboliche per non averle come indefined function)
%perch� deltaH0RdiT ha dentro sia T1 che T2 quindi devo collegarle tra loro per avare 3 equaz in 3 incognite
%HO UN SISTEMA DI 2 EQUAZ IN 2 INCOGNITE (LAMBDA E T)-->risolvo:
sol=solve(eq1,eq2,eq3,lambda,T1,T2); %mi restituisce le variabili simboliche a modi di operazioni inverse altrimenti rimangono simboliche nascoste, le devo definire per vedere i valori
lambdaRxn1=sol.lambda;
TTorcia1=sol.T1;
TTorcia=double(TTorcia1);
lambdaRxn=double(lambdaRxn1);

disp(['la temperatura adiabatica �', num2str(TTorcia),'K'])
disp(['il grado di avanzamento �',num2str(lambdaRxn)])