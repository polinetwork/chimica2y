function F=vanlaar(MB) %parametri di van laar si chiamano M e B

global antA antB antC M B

%dati azeotropo:
Taz=51.2;%�C
Paz=400; %torr
xaz=[0.399 0.601]; 

P0=10.^(antA+(antB./(antC+Taz)));%NB LE PARENTESI ANCHE CON + E - PER VETTORI
logP0=antA+(antB./(antC+Taz)); %p0 in torr e Taz in �C

%ora voglio uguagliare (equazione=0) i gamma calcolati con equilibrio fisico a quelli calcolati
%con relaz di van laar:
gamma1=exp(M/(1+(M*xaz(1))/(B*xaz(2)))^2);
gamma2=exp(B/(1+(B*xaz(2))/(M*xaz(1)))^2);
gamma=[gamma1 gamma2]; %vettore che contiene le gamma di 1 e 2 nella miscela az (funz di M,B)

%ora voglio F da azzerare per calcolare M e B: 
%gammaaz=Paz./P0 =vettore che contiene le gamma di 1 e 2 nella miscela az (numeri)
%-->((gammaaz*P0)/Paz)-1=0  
%-->CHIAMO F TUTTO CIO' CHE C'E'PRIMA DI = E LO AZZERO NEL MAIN CON 'FSOLVE'
F=((gamma.*P0)/Paz)-1; %� una funz vettoriale 
end



