clc
close all

format short g
%1=ETANANOLO
%2=BENZENE

global antA antB antC gamma1 gamma2 P x K
%% DATI
antA(1)=8.04494;
antB(1)=-1554.3;
antC(1)=222.65;

antA(2)=6.90565;
antB(2)=-1211.03;
antC(2)=220.79;

%calcolo valori di A e B per van laar
AB=[5 5]; %valori di primo tentativo anche se le incognite sono 2
AB= fsolve('vanlaar',AB);
A=AB(1);
B=AB(2);

%CALCOLO DELLA TBOLLA DATE A,B VAN LAAR
P=400; %torr
x(1)= 0.9;
x(2)=1-x(1); %ho definito il vettore x di composizione
gamma1=exp(A/(1+(A*x(1))/(B*x(2)))^2); %ora sono numeri perch� conosco A e B
gamma2=exp(B/(1+(B*x(2))/(A*x(1)))^2);

%ora apro uno script function con la funzione da azzerare

Tb=fsolve('Tbolla2',100);

%posso anche calcolare le y ora:
y=K.*x;




