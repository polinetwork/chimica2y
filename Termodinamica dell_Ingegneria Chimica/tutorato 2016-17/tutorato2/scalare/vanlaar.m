function [F]=vanlaar(AB)

global antA antB antC A B 

A=AB(1);
B=AB(2);

Taz= 51.2; %�C
Paz=400; %Torr
x1az=0.399;
x2az=1-x1az;

P01=10^(antA(1)+antB(1)/(antC(1)+Taz));
P02=10^(antA(2)+antB(2)/(antC(2)+Taz));

gamma1=exp(A/(1+(A*x1az)/(B*x2az))^2);
gamma2=exp(B/(1+(B*x2az)/(A*x1az))^2);

F(1)= P01*gamma1/Paz-1;
F(2)= P02*gamma2/Paz-1;
end

