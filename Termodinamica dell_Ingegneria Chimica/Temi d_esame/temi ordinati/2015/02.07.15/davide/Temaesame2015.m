%% TEMA ESAME 2015
% D. Lobriglio
clear all
close all
clc
global F z nu Keqs P1 Dh0f cpA cpB T0 G Keqf Ca S Vol k

%% REATTORE ALL'EQUILIBRIO
disp('Reattore all equilibrio')
R=8.314;    %J/mol/K
T0=600; %K
P1=1;   %bar
z=[0.25 0.15 0.6 0];
F=50/3600;  %mol/s
Dg0f=[-71.74 -88.52 -51.12 -406.33];    %J/mol
Dh0f=[-70.95 -94.47 -45.21 -310.52];    %J/mol
nu=[-1 -1 0 1];
cpA=[5.45 11.23 15.32 4.31]*4.186;      %J/mol/K
cpB=[0.021 0.0048 0.0072 0.0016]*4.186; %J/mol/K2
Trif=298.15;    %K

% Calcolo della Keq a Trif e Prif
Keq0=exp(-sum(nu.*Dg0f)/(R*Trif));

% Utilizzo un conto approssimato per valori di primo tentativo di T1 e l
% DH0R costante con la temperatura
DH0R=sum(nu.*Dh0f);
syms t T1
Keqs=Keq0*exp(int(DH0R/(R*t^2),Trif,T1));
X=fsolve('Temaesame2015_f',[700 0.4/3600]);
Tl=X;

% Calcolo effettivo di T1 e l
syms t T1
DH0R=sum(nu.*(Dh0f+cpA*(t-Trif)+cpB/2*(t^2-Trif^2)));
Keqs=Keq0*exp(int(DH0R/(R*t^2),Trif,T1));
X=fsolve('Temaesame2015_f',Tl);

T1=X(1);
l=X(2);
n=F*z+nu*l;
nt=sum(n);
x=n/nt;
disp(['Temperatura reattore = ' num2str(T1) ' [K]']);
disp(['Fraz molare A = ' num2str(x(1))]);
disp(['Fraz molare B = ' num2str(x(2))]);
disp(['Fraz molare I = ' num2str(x(3))]);
disp(['Fraz molare P = ' num2str(x(4))]);
disp(['Portata uscente dal reattore = ' num2str(nt) ' [mol/s]']);


disp(' ');
disp(' ');
disp(' ')
%% Separatore di fase
disp('Separatore di fase')
z=x;
T2=350; %K
P2=5;   %bar
G=nt;
gamma=[1.25 1.31 1 1.09];
ant1=[7.97882 6.89654 NaN 7.12387];
ant2=[-1234.56 -1346.12 NaN -1432.11];
ant3=[232 215 NaN 214];
P0=10.^(ant1+ant2./(T2-273.15+ant3))/760*1.01325;   %bar
P0(3)=15.3; %bar
Keqf=P0.*gamma/P2;

a=fsolve('Temaesame2015_f3',0.8);
x=(z)./(a*(Keqf-1)+1);
y=Keqf.*x;
V=a*G;
L=(1-a)*G;
disp(['Fraz molare A vapore = ' num2str(y(1))]);
disp(['Fraz molare B vapore = ' num2str(y(2))]);
disp(['Fraz molare I vapore = ' num2str(y(3))]);
disp(['Fraz molare P vapore = ' num2str(y(4))]);
disp(['Portata uscente dal reattore vapore = ' num2str(V) ' [mol/s]']);
disp(['Fraz molare A liquida = ' num2str(x(1))]);
disp(['Fraz molare B liquida = ' num2str(x(2))]);
disp(['Fraz molare I liquida = ' num2str(x(3))]);
disp(['Fraz molare P liquida = ' num2str(x(4))]);
disp(['Portata uscente dal reattore liquida = ' num2str(L) ' [mol/s]']);

disp(' ')
disp(' ')
disp(' ')
disp('Cinetica')
%% Reattore non all'equilibrio
O=10/3600;  %mol/s
S=V+O;
Vol=500;
nu=[-1 0 1 0 -1];
z(1:4)=V*y/S;
z(5)=O/S;
k=36.78;
x=fsolve('Temaesame2015_f4',[8 8 8 8 8],optimset('MaxFunEval',9000,'MaxIter',9000,'TolFun',1e-10));
Q=x(1);
nb=x(2);
ni=x(3);
np=x(4);
no=x(5);
na=S*0.05*Q^2/(Q^2+Vol*k*no*0.05);
n=[na nb ni np no];
nt=sum(n);
x=(n/nt)';
disp(['Concentrazione A = ' num2str(na/(Q*1e-3))]);
disp(['Concentrazione B = ' num2str(n(2)/(Q*1e-3))]);
disp(['Concentrazione I = ' num2str(n(3)/(Q*1e-3))]);
disp(['Concentrazione P = ' num2str(n(4)/(Q*1e-3))]);
disp(['Concentrazione O = ' num2str(n(5)/(Q*1e-3))]);
disp(['Tempo di residenza = ' num2str(Vol/Q)]);
