function f=Temaesame2015_f3(a)
global z Keqf
x=z./(a*(Keqf-1)+1);
f=sum(x)-1;