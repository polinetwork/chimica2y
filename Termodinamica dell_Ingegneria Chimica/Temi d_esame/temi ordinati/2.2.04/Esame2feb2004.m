clear all
close all
clc

%% DATI
% 1-S 2-R 3-A 4-P
R=8.314;
Pr=1;    %ATM
Tr=85+273.15;    %K
V=4;        %m3
mR=1500;    %kg
PMR=100;    %g/mol
P0Rs=0.002; %atm
L0=10000/3600;   %mol/s
z(3)=300/10000; %mol/s
z(1)=1-z(3);
z(2)=0;
z(4)=0;
nu=[0 -1 -1 1];
% stato di riferimento:gas a 298 K e 1 atm
Dg0fg=[-69.610 -44.290 -51.150 -112.920]*4186;  %J/mol
Dh0fg=[-68.320 -60.170 -21.880 -145.010]*4186;  %J/mol
Dhev=[50 43.8 31.0 23.0]*4186;  %J/mol
cp=[9.13 5.88 6.92 3.49]*4.186; %J/mol
ant1=[7.4 7.9 8.1 7.7];
ant2=[-1780.5 -1988.1 -1873.4 -1651.2];
ant3=[230 244 221 201];
P0=10.^(ant1+ant2./(Tr-273.15+ant3))/760;    %atm

%% Calcolo della costante di equilibrio
syms T
Dh0fgTr=Dh0fg+cp*(T-298);
Dg0fgRT=Dg0fg/(R*298)-int(Dh0fgTr./(R*T.^2),298,Tr);
Dg0fg=Dg0fgRT*R*Tr;
Dg0fg=vpa(Dg0fg);
Dg0fl=Dg0fg+R*Tr*log(P0);
DG0R=sum(nu.*Dg0fl);
Keq=exp(-DG0R/(R*Tr));
syms l
n=L0*z+nu*l;
nt=sum(n);
x=n/nt;
x(2)=P0Rs/P0(2);
l=solve(prod(x.^nu)==Keq,l);
l=vpa(l);
n=L0*z+nu*l;
nt=L0;
x=n/nt;
x(2)=vpa(P0Rs/P0(2));
fraz_molari_uscenti_dal_reattore=x'%scritta a video

%% Entalpia di reazione in fase liquida a Tr
DH0R=sum((Dh0fg+cp*(Tr-298)-Dhev).*nu)  %J/mol

%% Calcolo delle frazioni molari uscenti dal separatore
z=x;
L1=L0;
L2=L1*(z(3)+z(4));
L3=L1*(z(2)+z(1));
y=(L1*z(3:4)/L2);
x=(L1*z(1:2)/L3);
fraz_molari_uscenti_separatore_L2=y'
fraz_molari_uscenti_separatore_L3=x'

