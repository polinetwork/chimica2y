%Tema d'esame 15/02/06
clc
clear all

format short g

global Keq Pv Q fm0 conv l1 l2 l3

R=8.314; %J/mol/K
T=300; %K
DeltaG0f=[74.51 70.19 62.53 132.47 208.58].*1000; %J/mol
DeltaG0rea=[DeltaG0f(4)-(DeltaG0f(1)+DeltaG0f(2)), DeltaG0f(5)-(DeltaG0f(3)+DeltaG0f(4))]; %J/mol
Keq=exp(-DeltaG0rea./(R*T));

Pv = (exp(16.2243-(654.84/(T-7.16))))/760; %atm

Q=100; %mol
fm0=[0.35 0.35 0.3];

PT=[20 15 10];
options=optimset('Display','None');
ris=fsolve(@freaz,PT,options);

fprintf('\n Le moli di E gassoso prodotte sono: %g \n\n',ris(3))

PT2=[20 15 1];

for conv = 50:1:59
ris2=fsolve(@freaz2,PT2,options);
fprintf('\n La pressione necessaria per ottenere una conversione di C del %g%% e'': %g atm',conv,ris2(3))
end

l3 
l2
l1
