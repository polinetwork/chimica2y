close all
clear all
clc
format shortg

%dati [A B C D S]
global  Trif Tin nu1 nu2 H0Lfrif Cp n0
n0=[0.192 0.104 0 0 0.467]'; %mol/s
nu1=[-1 2 1 0 0];%reaz 1
nu2=[0 -1 -1 1 0];%reaz 2
p=1; %atm
Tin=423;%K
F=0.763; %mol/s
R=8.314;

Cp=[1.174 2.333 11.901 7.701 2.075]*4186; %J/mol K

Trif=298; %K
H0Lfrif=[-75500 37890 21130 -57800 -60300]*4.186; %J/mol  liquidi, a Trif



x=fsolve('sistema',[400 1 1])
