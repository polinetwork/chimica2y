function f=sistema(x);
global  Trif Tin nu1 nu2 H0Lfrif Cp n0 R

T=x(1);
l1=x(2);
l2=x(3);

%per bilancio en:
Cptot=sum(n0'.*Cp); %scalare
DCp1=sum(nu1.*Cp);%scalare
DCp2=sum(nu2.*Cp);
DH0RL1rif=sum(nu1.*H0Lfrif);
DH0RL2rif=sum(nu2.*H0Lfrif);
DH0RL1=DH0RL1rif+DCp1*(T-Trif);
DH0RL2=DH0RL2rif+DCp2*(T-Trif);

%per eq chimici:
l=[l1 l2];
nu=[nu1;
 nu2]'
n=n0+nu.*l;
ntot=sum(n);
x=n/ntot;
xA=x(1);
xB=x(2);
xC=x(3);
xD=x(4);
xS=x(5);

gamma=[1 1.5*xC^2 1.5*xD^2 1];

att=x.*gamma;

Keq1_att=prod(att.^nu1);
Keq2_att=prod(att.^nu2;

%Van't Hoff:
P0=exp(A-(B/C+T)/760;
G0frif=[-14530+86*(T-273); -15770+112*(T-273)];
G0RVrif=sum(nu.*G0frif)
G0RLrif=G0RVrif+(R*T*log((P0).^nu));
G0RL_RT=G0RLrif/(R*Trif);
Keq_VH=exp(-G0RL_RT);

Keq_1VH=Keq_VH(1);
Keq_2VH=Keq_VH(2);

f(1)=Cptot*(Tin-T)-l1*DH0RL1-l2*DH0RL2; %bilancio en
f(2)=Keq1_att-Keq1_VH; %eq chimici
f(3)=Keq2_att-Keq2_VH;