function f=esame2014_sistema(l)
global n0V n0L nuL p H Keq_VH
l1=l(1);
lO=l(2);
lN=l(3);
%l=[l1 lO lN] incognite con l1=reaz, lO,lN=eq fisico

%[O2 N2 CHO COOH]
%fase vapore:
nV(1)=n0V(1)+lO;
nV(2)=n0V(2)+lN;
nV(3)=n0V(3);
nV(4)=n0V(4);
nVtot=sum(nV);
y=nV/nVtot;

%fase liquida:
nL(1)=n0L(1)-lO+nuL(1)*l1;
nL(2)=n0L(2)-lN+nuL(2)*l1;
nL(3)=n0L(3)+nuL(3)*l1;
nL(4)=n0L(4)+nuL(4)*l1;
nLtot=sum(nL);
x=nL/nLtot;
%eq chimico:
produttoria2=prod(x.^nuL)^2;
Keq_att=sqrt(produttoria2);

%sistema:
f(1)=Keq_att-Keq_VH;
%f(2)=p*y(1)-H(1)*x(1);
f(3)=p*y(2)-H(2)*x(2);
f(2)=sum(x)-sum(y);


