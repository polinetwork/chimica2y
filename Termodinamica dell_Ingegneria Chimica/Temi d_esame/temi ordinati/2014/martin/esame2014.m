function y=esame2014(x)

global P Trz R DG0rz HO2 HN2 F1 F2 zV 

L=x(1);
V=x(2);
yO2=x(3);
yN2=x(4);
xO2=x(5);
xN2=x(6);
xCHO=x(7);
xCOOH=x(8);
lambda=x(9);

y(1)= (xCOOH/(xCHO*xO2^(0.5)))-exp(-DG0rz/R*Trz);
y(2)= P*yO2-HO2*xO2;
y(3)= P*yN2-HN2*xN2;
y(4)= F1+F2-V-L-0.5*lambda;
y(5)= yO2+yN2-1;
y(6)= F1-xCHO*L-lambda;
y(7)= F2*zV(1)-yO2*V-xO2*L-0.5*lambda;
y(8)= xCOOH*L-lambda;
y(9)= F2*zV(2)-yN2*V-xN2*L;

