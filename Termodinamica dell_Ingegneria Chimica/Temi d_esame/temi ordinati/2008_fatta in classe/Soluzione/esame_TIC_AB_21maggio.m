%--------------------------------------------------------------------------
%ESAME TERMO I                                                        md&ms
%--------------------------------------------------------------------------

clc
clear all
close all
format compact 

global k_fis_all_id F Aria yaria xF a b R T phi_vap_puro_Pev P A B


%--------------------------------------------------------------------------
% D A T I
%--------------------------------------------------------------------------
R=8.314; %[J/molK]
%  Condizioni Operative
F = 100;    %[mol/h]
Aria=200;   %[mol/h]
P = 1;      %[atm] --> non converto in Pa dato che nelle relazioni di eq.
            %          ho solo rapporti tra pressioni e nei parametri A e B
            %          ho nuovamente un rapporto tra pressione e Pc
Tin=273+25; %[k]

% Composizione Aria
%     [THF   H2O   O2    N2  ] 
yaria=[0     0     0.21  0.79];
% Composizione Feed
%     [THF   H2O   O2    N2  ] 
xF   =[0.3   0.7   0     0   ];
%--------------------------------------------------------------------------
%  Parametri Critici
%  [THF   H2O    O2    N2]
Tc=[540   647    32.97 126.21];
Pc=[51.09 218.11 12.76 33.46];

%  Antoine
%    [THF      H2O     ] 
antA=[4.12118  5.20389 ];
antB=[1202.942 1733.926];
antC=[-46.818  -39.485 ];

%  Costanti di Henry
%     [O2      N2     ] [atm]
henry=[4.259E4 9.077E4];


%  Parametri Critici
%  [THF   H2O    O2    N2]
Tc=[540   647    32.97 126.21];
Pc=[51.09 218.11 12.76 33.46];





%--------------------------------------------------------------------------
% Stampa info su file e a video
%--------------------------------------------------------------------------
RIS = fopen('RisultatiEsame.txt','w');
fprintf(RIS,'\n************************************************ \n');
fprintf(RIS,'ESAME DI "TERMODINAMICA DELL INGEGNERIA CHIMICA" \n');
fprintf(RIS,'21 Maggio 2008                               ms\n');                      
fprintf(RIS,'************************************************\n\n');
fprintf('\n************************************************ \n');
fprintf('ESAME DI "TERMODINAMICA DELL INGEGNERIA CHIMICA" \n');
fprintf('21 Maggio 2008                               ms\n');                      
fprintf('************************************************\n\n');
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
%Ciclo su tutti i numeri di matricola
%--------------------------------------------------------------------------

for i=0:1:9
    
%  Dati funzione del numero di matricola
x=i;
T=273+40+2*x; %[K]

% Paramtri funzione di T

% a e b per le specie pure
a=(0.42748.*(R.*Tc).^2)./sqrt(Pc.*(T./Tc));
b=(0.08664.*(R.*Tc)./Pc);

% Pev per i componenti subcritici
Pev =10.^(antA-(antB./(T+antC)));   %[K],[atm]


%  Costanti di ripartizione L/V per miscela ideale di componenti ideali
%  [THF   H2O    O2    N2]
k_fis_all_id=[Pev./P henry./P]; 


% Paramtri della cubica@(T,P) per ogni componente PURO
% calcolo i parametri A e B indipendenti dalle composizioni da passare alla 
% funzione da azzerare
A=a.*P./((R.*T).^2);
B=b.*P./(R.*T);


% Cubica@(T,Pev) per ogni componente PURO non ideale (THF e H2O)
Apev=a(1:2).*Pev./((R.*T).^2);
Bpev=b(1:2).*Pev./(R.*T);
uno=[1 1]';
alfa=[-1 -1]';
beta=(Apev-Bpev-Bpev.^2)';
gamma=(-Apev.*Bpev)';
coeff=[uno alfa beta gamma];
% Z_vap@(T,Pev) per THF e H2O
% la sintassi coeff(i,:) indica la riga i-esima della matrice coeff
z_vap_Pev=[ max(real(roots(coeff(1,:))))  max(real(roots(coeff(2,:))))]; 
% Phi@(T,Pev) per ogni componente PURO non ideale (THF e H2O)
phi_vap_puro_Pev=exp(z_vap_Pev-1-(Apev./Bpev).*log((z_vap_Pev+Bpev)/z_vap_Pev)-log(z_vap_Pev-Bpev));


%--------------------------------------------------------------------------
% Primo punto
%--------------------------------------------------------------------------
% Incognite >>==> L, V, xL, yV
% passo i valori di primo tentativo di L, V, yV, xF 
%
% Calcolo i valori di primo tentativo trascurando i phi_i e gamma_i per le
% specie THF e wat
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% Calcolo i valori di primo tentativo trascurando tutte le non idealita'
%--------------------------------------------------------------------------

fprintf(RIS,'\t\t\t<<<< X = %d >>>>\t\t\tINIZIO\n',x);

fprintf(RIS,'\n***************************************');   
fprintf(RIS,'\nTutto ideale\n');
fprintf(RIS,'***************************************\n');

guess=[ 100 200 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 ];

options=optimset('Display','off');

sol = fsolve('sistema_mix_id_gas_id',guess,options);

%--------------------------------------------------------------------------
% Stampa dei risultati
%--------------------------------------------------------------------------

L_1=sol(1);
V_1=sol(2);
for i=1:4
    xL_1(i)=sol(i+2);
    yV_1(i)=sol(i+6);
end
fprintf(RIS,'\nL=%d\t[mol/h]\nV=%d\t[mol/h]\n\nxL\n THF: %d\n H2O: %d\n O2: %d\n N2: %d\n\nyV\n THF: %d\n H2O: %d\n O2: %d\n N2: %d\n\n',L_1,V_1,xL_1,yV_1);
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% Considero solo i coeff. di attivita', trascuro le Phi
%--------------------------------------------------------------------------

fprintf(RIS,'\n***************************************');
fprintf(RIS,'\nMix non ideale di componenti ideali\n');
fprintf(RIS,'***************************************\n');

guess=[L_1 V_1 xL_1 yV_1];

sol = fsolve('sistema_non_id',guess,options);

%--------------------------------------------------------------------------
% Stampa dei risultati
%--------------------------------------------------------------------------

L_2=sol(1);
V_2=sol(2);
for i=1:4
    xL_2(i)=sol(i+2);
    yV_2(i)=sol(i+6);
end
fprintf(RIS,'\nL=%d\t[mol/h]\nV=%d\t[mol/h]\n\nxL\n THF: %d\n H2O: %d\n O2: %d\n N2: %d\n\nyV\n THF: %d\n H2O: %d\n O2: %d\n N2: %d\n\n',L_2,V_2,xL_2,yV_2);
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% Considero tutte le non idealita'
%--------------------------------------------------------------------------

fprintf(RIS,'\n***************************************');
fprintf(RIS,'\nMix non ideale di componenti non ideali\n');
fprintf(RIS,'***************************************\n');

guess=[L_1 V_1 xL_1 yV_1];

sol = fsolve('sistema_tutto_non_id',guess,options);

%--------------------------------------------------------------------------
% Stampa dei risultati
%--------------------------------------------------------------------------
L_3=sol(1);
V_3=sol(2);
for i=1:4
    xL_3(i)=sol(i+2);
    yV_3(i)=sol(i+6);
end
fprintf(RIS,'\nL=%d\t[mol/h]\nV=%d\t[mol/h]\n\nxL\n THF: %d\n H2O: %d\n O2: %d\n N2: %d\n\nyV\n THF: %d\n H2O: %d\n O2: %d\n N2: %d\n\n',L_3,V_3,xL_3,yV_3);
%--------------------------------------------------------------------------


%--------------------------------------------------------------------------
% Punto due 
%--------------------------------------------------------------------------
% Calcolo Int(CpdT) tra Tin e T
%
CpA=[18.44 7.701     6.713     7.440];
CpB=[0     4.595E-4 -0.878E-2  -0.324E-2];
CpC=[0     7.440    -0.324E-2  6.4E-6];


intcp=(CpA.*(T-Tin)+1/2.*CpB.*(T^2-Tin^2)+1/3.*CpC.*(T^3-Tin^3)).*4.186; % [j/mol]

%--------------------------------------------------------------------------
% Calcolo hr per le correnti in fase liquida e in fase vapore
%
%--------------------------------------------------------------------------
% zliq F e hr F
%--------------------------------------------------------------------------
amix=(sum(xF.*sqrt(a))).^2;
bmix=sum(xF.*b);
Amix=amix.*P/((R.*T).^2);
Bmix=bmix.*P./(R.*T);
alfa=-1;
beta=(Amix-Bmix-Bmix.^2);
gamma=(-Amix.*Bmix);
coeff=[1 alfa beta gamma];
z_liq_F=min(real(roots(coeff)));

hr_F=(z_liq_F-1-(3.*Amix./(2.*Bmix)).*log((z_liq_F+Bmix)./z_liq_F)).*R.*T;

%--------------------------------------------------------------------------
% zliq L e hr L
%--------------------------------------------------------------------------
amix=(sum(xL_3.*sqrt(a))).^2;
bmix=sum(xL_3.*b);
Amix=amix.*P/((R.*T).^2);
Bmix=bmix.*P./(R.*T);
alfa=-1;
beta=(Amix-Bmix-Bmix.^2);
gamma=(-Amix.*Bmix);
coeff=[1 alfa beta gamma];
z_liq_L=min(real(roots(coeff)));

hr_L=(z_liq_L-1-(3.*Amix./(2.*Bmix)).*log((z_liq_L+Bmix)./z_liq_L)).*R.*T;

%--------------------------------------------------------------------------
% zvap Aria e hr Aria
%--------------------------------------------------------------------------
amix=(sum(yaria.*sqrt(a))).^2;
bmix=sum(yaria.*b);
Amix=amix.*P/((R.*T).^2);
Bmix=bmix.*P./(R.*T);
alfa=-1;
beta=(Amix-Bmix-Bmix.^2);
gamma=(-Amix.*Bmix);
coeff=[1 alfa beta gamma];
z_vap_Aria=max(real(roots(coeff)));

hr_Aria=(z_vap_Aria-1-(3.*Amix./(2.*Bmix)).*log((z_vap_Aria+Bmix)./z_vap_Aria)).*R.*T;

%--------------------------------------------------------------------------
% zvap V e hr V
%--------------------------------------------------------------------------
amix=(sum(yV_3.*sqrt(a))).^2;
bmix=sum(yV_3.*b);
Amix=amix.*P/((R.*T).^2);
Bmix=bmix.*P./(R.*T);
alfa=-1;
beta=(Amix-Bmix-Bmix.^2);
gamma=(-Amix.*Bmix);
coeff=[1 alfa beta gamma];
z_vap_V=max(real(roots(coeff)));

hr_V=(z_vap_V-1-(3.*Amix./(2.*Bmix)).*log((z_vap_V+Bmix)./z_vap_V)).*R.*T;


Q=V_3.*(sum(yV_3.*intcp)+hr_V)+L_3.*(sum(xL_3.*intcp)+hr_L)-F.*(sum(xF.*intcp)+hr_F)-Aria.*(sum(yaria.*intcp)+hr_Aria);

fprintf(RIS,'\n***************************************');
fprintf(RIS,'\nPotenza Q=%d [kW]\n',Q/(1E3*3600));
fprintf(RIS,'***************************************\n');

fprintf(RIS,'\t\t\t<<<< X = %d >>>>\t\t\tFINE\n\n\n',x);
end
fclose(RIS);
fprintf('\nI risultati sono contenuti per tutti i numeri di matricola nel file RisultatiEsame.txt\nPer visualizzarlo scrivi "type RisultatiEsame.txt"\n');