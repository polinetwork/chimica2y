function[F]=Esame1_function(T)
global P y ant1 ant2 ant3
P0=10.^(ant1-ant2./(ant3+T))*133.322e-5;    %Pa
F=sum((P*y)./P0)-1;

