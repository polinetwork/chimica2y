function g=sist1(x)

global P T no nu DGor

lambda=x;

R=8.314472; %J/mol/K

n=no+nu*lambda;
ntot=sum(n);

xa=n(1)/ntot;
xb=n(2)/ntot;
xc=n(3)/ntot;
xd=n(4)/ntot;

g=xd/xc/xa^2/P^2-exp(-DGor/R/T);

