clc
clear all

global y p antA antB antC kc V F2 wA wB wC wD G
%% FLASH SENZA REAZIONE
%dati: 1=A, 2=B
F1=10000; %mol/h
z=[0.6 0.4];
p=2.5; %bar
y=[0.7 0.3];
antA=[7.98097 7.91310];
antB=[1582.271 1730.630];
antC=[239.726 233.426];

%calcolo della Tflash
T=fsolve('Tflash',110,optimset('maxfunevals',9000,'maxiter',9000,'tolfun',1e-10)); %K
%ora calcolo le altre incognite:
p0=(10.^(antA-antB./(antC+T-273.15)))/750; %bar
x=(p./p0).*y;
L=(F1*(z-y))/(x-y);
V=F1-L;

%% REATTORE
%dati:
R=8.314; %j/mol*K
F2=2000; %mol/h
%SBAGLIATISSIMOOOOOO G=V+F2; 
n0A=V*y(1); %mol/h
n0B=V*y(2);
n0=[n0A n0B F2 0];
DG0r=-12e+03; %j/mol
DG0rRT=DG0r/(R*T);
kc=exp(-DG0rRT);

%funz da azzerare per trovare lambda
l=fsolve('eqchimico',0.876);

%calcolo delle portate in uscita:
w=[wA wB wC wD];



