function f=sistema(x)

%risolvo sistema
options=optimset('display','iter','tolfun',1e-10,'maxiter',3e+3,'maxfunevals',10000);
[d,fval,exitflag]=fsolve('sistema',[5000; 5000; 0.5; 0.5; 300;],options); %cos� risolve tenendo conto delle opzioni che ho definito prima
disp(['La portata di vapore �',num2str(d(1)),'mol/h']);%2str1 riporta il risultato numerico che ottiene facnedo girare il sistema poi ci aggiunge l'etichetta
disp(['La portata di liquido �',num2str(d(2)),'mol/h']);
disp(['La frazione molare xA di liquido �',num2str(d(3))]);
disp(['La frazione molare xB di liquido �',num2str(d(4))]);
disp(['La temperatura del flash �',num2str(d(5))]);

global z y F1 antA antB antC
%dichiaro le incognite:
V=x(1);
L=x(2);
xA=x(3);
xB=x(4);
T=x(5);

%definisco p0 in forma vettoriale
p0=[p0A p0B];
p0=(antA-(antB./(T+273+antC)))/750; %bar (con T output in K)


%scrivo le equaz da azzerare (una in forma vettoriale)
f=[F1-V-L; F1*z(1)-V*y(1)-L*xA; p0A*xA-p*y(1); p0B*xB-p*y(2); xA+xB-1;]