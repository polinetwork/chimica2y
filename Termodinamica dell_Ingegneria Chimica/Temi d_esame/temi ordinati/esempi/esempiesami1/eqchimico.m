function F=eqchimico(l) %l=lambda
global p kc V y F2 wA wB wC wD G
G=V+F2-2*l; %oppure semplicemente: G=V+F2-2*l
wA=((V*y(1))-2*l)/G;
wB=(V*y(2))/G;
wC=(F2-l)/G;
wD=l/G;

F=(wD/((p*wA)^2)*wC)-kc;
