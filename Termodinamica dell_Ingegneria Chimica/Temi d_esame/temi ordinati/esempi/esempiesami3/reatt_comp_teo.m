function F=reatt_comp_teo(l)

global F1 F2 P2 phiC2 phiA2 phiB2 Keq_T2

%Calcolo K equilibrio come produttoria delle attivit�

Keq_a=1/(P2/1e5)^2*phiC2/(phiB2*phiA2^2)*l*(F1+F2-2*l)^2/((F2-l)*(F1-2*l));

F=Keq_T2-Keq_a;