clear all
clc
format short E

global DG1 DG2 no nu 

% Dati:
P=5; %bar
T=110; %�C
DG1=-12530+86*T; %J/mol %T[�C]
DG2=-15770+112*T; %J/mol e T[�C]

nS1=10000/40; %moli
nS2=15000/152; %moli
nAo=10000/108; %moli
nBo=5000/48; %moli

% S=[Aa Ba Ca S1a Bb Cb Pb S2b] - vettore ordine delle specie 

no=[nAo nBo 0 nS1 0 0 0 nS2]; % vettore numero moli iniziali 
nu=[-2 1 1 0 0 0 0 0;0 0 0 0 -1 -1 1 0; 0 -1 0 0 1 0 0 0; 0 0 -1 0 0 1 0 0]; % vettore dei coeff. stechiometrici delle 4 reazioni 

% Soluzione del sistema:
xo=[0.5 .5 1 1];
f=fsolve('soluzione',xo);
display('lambda 1-4'),f'
n=no+f*nu;
na=n(1:4);
nb=n(5:8);
nta=sum(na);
ntb=sum(nb);
display('frazioni molari fase alfa'),(na/nta)'
display('frazioni molari fase beta'),(nb/ntb)'

sommatoria_Xalfa=sum(na/nta)
sommatoria_Xbeta=sum(nb/ntb)
