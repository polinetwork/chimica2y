function c=eqchimico(l)
global nu n0 V F2 Keq 
n=n0+nu.*l;
G=V+F2-l;
w=n./G;
p=2.5;
att=p*w;

Keq_att=(prod(att).^nu);
c=Keq_att-Keq;
