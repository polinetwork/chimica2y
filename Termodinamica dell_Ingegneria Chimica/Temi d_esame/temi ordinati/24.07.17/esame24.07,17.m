close all
clear all
clc
format shortg
global F z p p0 
%% FLASH 
%dati: [A B]
T=373.02; %K
p=2.5; %bar
F=10; %mol/s
z=[0.6 0.4];

antA=[7.98097 7.91310];
antB=[1582.271 1730.630];
antC=[239.726 233.426];
p0=(10.^(antA-antB./(T-273+antC)))./760;%bar

x0=[0.5 0.5 0.5 0.5 0.001 0.001];
x=fsolve('flash_sistema',x0);
xA=x(1)
xB=x(2)
yA=x(3)
yB=x(4)
V=x(5)
L=x(6)

%% REATTORE
global n0 nu V F2 Keq
%dati:[A B C]
F2=1; %mol/s
nu=[1 -1 -1]';
DG0R=-13*1e3; %j/mol
R=8.314; %J/mol K
Keq=exp(-DG0R/(R*T));
n0=[V*yA V*yB F2]';

l=fsolve('eqchimico',-3);
n=n0+nu.*l;
G=V+F2-l;
w=n./G
G

sum(w);
yA+yB;
xA+xB;

