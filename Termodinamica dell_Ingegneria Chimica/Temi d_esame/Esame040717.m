clc; clear all; close all;

global A B C R Tr Tin cl nu1 nu2 dh0fL z nin na nb ns
delta=0;
Vs=(15+delta)/100; %m3/h
Va=1.8; %kg/s
Vb=3e-3; %kg/s
ros=1.5; %kg/L
MWs=0.152; MWa=156; MWb=48;
ns=Vs*ros*1000/3600/MWs;
na=Va/60*1000/MWa; %mol/s
nb=Vb/MWb*1000;
nin=(ns+na+nb)*3600; %mol/h
Tin=423.15; %K
P=1.01325; %bar
z=[na/nin nb/nin 0 0 ns/nin];
dh0fL=[-75500 37890 21130 -57800 -60300]*4.186; %J/mol
A=[12.926 17.9899 15.8441 18.3036 16.5407];
B=[1872.45 3877.65 2582.32 3816.44 3016.89];
C=[25.16 45.15 51.56 46.13 54.14]*(-1);
cl=[1.174 2.333 11.901 7.701 2.075]*4186; %J/mol/K
% DG0R1gas=-14530+86*T; %J/mol  T=[�C]
% DG0R2gas=-15770+112*T;
nu1=[-1 2 1 0 0]; %[A B C D S]
nu2=[0 -1 -1 1 0];
R=8.314; %J/mol/K
Tr=25; %�C

% soluzione
sol0=[350 50 30];
options=optimset('MaxFunEvals',30000,'MaxIter',30000,'TolX',1e-7,'TolFun',1e-7);
sol=fsolve(@sistema,sol0,options);
T=sol(1)
lam1=sol(2)
lam2=sol(3)

function f=sistema(m)
global A B C R Tr Tin cl nu1 nu2 dh0fL z nin na nb ns
T=m(1); lam1=m(2); lam2=m(3);

nout=(nin+2*lam1-lam2);
x=[(na-lam1)/nout (nb-2*lam1-lam2)/nout (lam1-lam2)/nout lam2/nout ns/nout];
P0=exp(A-B./(T+C))*133.3/1e5; %bar, T=[K]
gamma=[1 1.5*x(3)^2 1.5*x(2)^2 1 0]; %[A B C D S]
ai=x.*gamma;
DH01=sum(nu1.*dh0fL);
DH02=sum(nu2.*dh0fL);
DG0R1R=(-14530+86*Tr)+R*298*log(prod(P0.^(abs(nu1))));
DG0R2R=(-15770+112*Tr)+R*298*log(prod(P0.^(abs(nu2))));

f(1)=prod(ai.^nu1)-(exp(-DG0R1R/R/298)*exp((1/298-1/T)*(DH01-sum(cl*298))/R+(sum(nu1.*cl))/R*log(T/298)));
f(2)=prod(ai.^nu2)-(exp(-DG0R2R/R/298)*exp((1/298-1/T)*(DH02-sum(cl*298))/R+(sum(nu2.*cl))/R*log(T/298)));
% f(3)=sum(x)-1;
% f(4:8)=nin*z-nout*x+nu1*lam1+nu2*lam2;
f(3)=nin*(sum(z.*(dh0fL+cl*(Tin-298))))-nout*(sum(x.*(dh0fL+cl*(T-298))));
end






