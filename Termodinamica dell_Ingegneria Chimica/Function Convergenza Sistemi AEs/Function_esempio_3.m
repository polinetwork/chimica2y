%fmain_01 Esempio di equazioni risolte con fsolve_HC
%         x1^2-1=0; x2^2-0.25=0; con vincoli 0<x<1

% R. Rota (2020)

function F=fmain_01(x)

f(1) = x(1)^2-1;
f(2) = x(2)^2-0.25;
f(3) = x(1)*(x(1)-1)+x(3)^2;
f(4) = x(2)*(x(2)-1)+x(4)^2;

F  = f;