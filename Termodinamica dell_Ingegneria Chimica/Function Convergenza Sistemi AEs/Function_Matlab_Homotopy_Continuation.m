%fsolve_HC  risoluzione di AEs con Homotopy Continuation
%           Jim�nez-Islas et al., "Nonlinear Homotopic Continuation Methods: 
%           A Chemical Engineering Perspective Review", 
%           Ind. Eng. Chem. Res. 2013, 52, 14729-14742
%           https://doi.org/10.1021/ie402418e

% Chiamata: [x res] = fsolve_HC (@fcn, x0, dt, imet)

% Output:
% x       valori delle radici del AEs
% res     valori dei residui delle equazioni (per verifica risultati)

% Input:
% fcn     function in cui si calcolano i residui delle AEs (come per fsolve)
% x0      valori di primo tentativo delle radici del AEs
% dt      passo di evoluzione della traiettoria nell'intervallo [0 1] 
%           Valori piccoli facilitano la ricerca delle radici e aumentano 
%           il tempo di calcolo; valore suggerito: 1E-2
% imet    metodo utilizzato:
%           imet=1  Fixed point homotopy
%           imet=2  Newton homotopy
%           imet=3  Fixed point - Newton homotopy

% R. Rota (2020)

% Caveat! Anything free comes with no guarantee.

function [x Residui] = fsolve_HC (fcn, x0, dt, imet)

y = [];
y0= x0;
switch (imet)
  case 1
    for t=0:dt:1
      f_FP = @(y) t.*fcn(y)+(1-t).*(y-x0);
      x    = fsolve(f_FP,y0);
      y0   = x;
    endfor
    Residui= f_FP(x);
  case 2
    for t=0:dt:1
      f_N  = @(y) t.*fcn(y)+(1-t).*(fcn(y)-fcn(x0));
      x    = fsolve(f_N,y0);
      y0   = x;
    endfor
    Residui= f_N(x);
  case 3
    for t=0:dt:1
      f_FPN  = @(y) t.*fcn(y)+(1-t).*(y-x0+fcn(y)-fcn(x0));
      x    = fsolve(f_FPN,y0);
      y0   = x;
    endfor
    Residui= f_FPN(x);
  otherwise
    disp('wrong method number');
end



