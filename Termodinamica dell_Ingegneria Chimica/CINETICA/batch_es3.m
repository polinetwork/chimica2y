function yp=batch_es3(t,y)
global k nu

A=y(1);
B=y(2);
C=y(3);
R=[ k(1)*A^2;  
k(2)*B];

r=nu*R;
yp=r;


