function yp = fBatch_1(t,y)
% fBatch_1
% reattore BATCH/PFR - Esercizio 1
% R. Rota 2013
global k v
A = y(1);
R = y(2);
B = y(3);
C = y(4);
D = y(5);
R = [ k(1)*A;
k(2)*B*R];
r = v*R;
yp = r;