function dCdt=megaODE_f2(t,y)
global C0 k1 k2
A=y(1);
B=y(2);
C=y(3);
dCdt(1)=-(k1+k2)*A;
dCdt(2)=k1*A;
dCdt(3)=k2*A;
dCdt=dCdt';