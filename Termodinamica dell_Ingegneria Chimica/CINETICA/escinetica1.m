%BATCH: A-->B
global nu k
nu=[-1 1]'; %[A B]
d=800; %Kg/m3
MW=32; %kg/kmol
y0A=(d/MW)*1e3; %mol/m3
y0=[y0A 0]; %mol/m3
k0=(2.5*1e5)/((1e3)*3600); %m3/mol s
Ea=32230.66; %J
R=8.314; %J/mol K
T=398; %K
k=k0.*exp(Ea/(R*T)); %m3/mol s
[t,y]=ode23('Batch_es1',[0 tau],y0);
yF=y(length(t),:)